# -*- coding: utf-8 -*-
"""
Stpg-bot discord bot.

Extends the functionality of discord for the /wg/ server.
"""
import os

from dotenv import load_dotenv

# Checks if running on Heroku.
if not "Dyno" in os.environ:
    """Local development environment variables."""
    load_dotenv(override=True)

from discord.ext import commands

from utils.logger import logger

initial_extensions = [
    "cogs.config",
    "cogs.pinbot",
    "cogs.embeds",
    "cogs.anonChat",
    "cogs.welcome",
    "cogs.topics",
    "cogs.playing",
    "cogs.based",
]

BOT = commands.Bot(command_prefix=os.getenv("BOT_PREFIX") or "?")

if __name__ == "__main__":
    for extension in initial_extensions:
        try:
            logger.info(f"{extension} loading...")
            BOT.load_extension(extension)
            logger.info(f"{extension} loaded!")
        except Exception as e:
            logger.error(
                "Failed to load extension {}\n{}: {}".format(
                    extension,
                    type(e).__name__,
                    e,
                )
            )

    BOT.run(os.getenv("BOT_TOKEN"))
