# stpg-bot  

Discord bot created to extend discord functionality for the /wg/ discord server.

## Current cogs  

* **pinbot**: responsible for automatically sending messages that have reached a
certain number of reactions to a "pin" channel.
* **embeds**: hardcoded embeds to be used for server information messages.
* **anonChat**: a quick and hacky way to anonymously send messages into a
channel via direct messaging the bot.
* **welcome**: sends a customisable welcome message for each new user.

### Adding extra cogs

This bot attempts to make each cog responsible for as much of it's own functionality as possible.
Each cog needs to set the `valid_settings` property on the `config` cog to the appropriate valid
setting names it requires for it's use. See `welcome.py` for it's use in the welcome cog.

## Technology  

The tech-stack in this project should be kept to a complete minimum, working off
of PostgreSQL with Discord.py and installing packages where absolutely necessary
or time-saving.

The "bot" itself is entirely functionless and should rely on the loading of
"cogs" in order to extend this functionality. Each "cog" should ideally stick to
a singular functionality i.e `pinbot.py` is responsible solely for the pinning
of messages and it's configuration.

## Running the bot  

* (optional) Set up a virtual python environment i.e `python3 -m venv venv`
* Install package requirements `pip install requirements.txt`
* Run using `python3 main.py`

## Development setup  

Create a virtual environment (venv):

* `python3 -m venv env`
* `source env/bin/activate`

If you install additional packages:

* `pip freeze > requirements.txt`
