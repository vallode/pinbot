import re

import discord
from discord.ext import commands

from utils.config import config
from utils.logger import logger


class AnonChat(commands.Cog):
    """
    Cog for creating anonymous messages through users direct messaging the bot.
    """

    def __init__(self, bot) -> None:
        self.bot = bot

        self.configCog = self.bot.get_cog("Config")
        self.configCog.valid_settings = ["anon_channel"]
        self.anon_channel = config.get_or_set("anon_channel", "BIGINT")

    @commands.Cog.listener(name="on_message")
    async def _on_message(self, message: discord.Message):
        if message.author.id == self.bot.user.id:
            return

        if not isinstance(message.channel, discord.DMChannel):
            return

        channel = self.bot.get_channel(self.anon_channel)

        # Find and replace all mentions with the mention of the bot itself.
        message_content = re.sub(
            r"(<@(\u200b)?![0-9]*>)",
            f"<@{self.bot.user.id}>",
            discord.utils.escape_mentions(message.content),
        )

        return await channel.send(message_content)


def setup(bot: commands.Bot):
    bot.add_cog(AnonChat(bot))
