import random
import re

import discord
from discord.ext import commands

from utils.config import config
from utils.db import db
from utils.logger import logger

BASED = [
    "based",
    "based and redpilled.",
    "super based",
    "pretty based bro",
    "totally based",
    "based and wgpilled",
]

NOT_BASED = [
    "cringe bro",
    "ew... cringe",
    "no",
    "unbased and not pilled at all",
]


class Based(commands.Cog):
    """
    Fun cog currently only responsible for simple replies.
    A user can be based, or not based. The bot will respond in kind.
    Users can call others based, but only based users can make other users based.

    Self-explanatory.
    """

    def __init__(self, bot) -> None:
        self.bot = bot

        self.configCog = self.bot.get_cog("Config")

    def _set_user_based(self, user: discord.Member, based) -> discord.Member:
        db.execute(
            (
                "INSERT INTO discord_user(user_id, based) "
                "VALUES (%(user_id)s, %(based)s) "
                "ON CONFLICT (user_id) DO "
                "UPDATE SET based = %(based)s::boolean"
            ),
            {"based": based, "user_id": user.id},
        )

        return user

    @commands.Cog.listener(name="on_message")
    async def _on_message(self, message: discord.Message):
        """
        Checks every message to look for repeating digits at the end of it's snowflake ID.
        Does not check bot or DM messages.
        """
        if message.author.id == self.bot.user.id:
            return

        if isinstance(message.channel, discord.DMChannel):
            return

        match = re.search(r"(\d)\1{3,}$", str(message.id))

        if match:
            self._set_user_based(message.author, True)
            return await message.channel.send("dude... that's pretty based.")

    @commands.command()
    async def based(self, ctx, user: discord.Member) -> None:
        """
        Give based status to another user.
        Can only be used if you are based yourself.
        """
        if user.id == self.bot.user.id:
            return await ctx.send("I'm already based as fuck.")

        if ctx.author.id == user.id:
            self._set_user_based(user, False)

            return await ctx.send(
                "dude... are you trying to base yourself? pretty cringe bro"
            )

        user_based = db.select_rows(
            ("SELECT based " "FROM discord_user " "WHERE user_id = %s::varchar"),
            (ctx.author.id,),
        )[0]

        if not user_based:
            return await ctx.send("bro... based on what?")

        self._set_user_based(user, True)

        return await ctx.send(random.choice(BASED))

    @commands.command()
    async def isbased(self, ctx, user: discord.Member) -> None:
        """
        Check if a user is based.
        """
        if user.id == self.bot.user.id:
            return await ctx.send("I'm based as fuck.")

        user_based = db.select_rows(
            ("SELECT based " "FROM discord_user " "WHERE user_id = %s::varchar"),
            (user.id,),
        )

        if user_based and user_based[0]:
            return await ctx.send(random.choice(BASED))
        else:
            return await ctx.send(random.choice(NOT_BASED))


def setup(bot: commands.Bot):
    bot.add_cog(Based(bot))
