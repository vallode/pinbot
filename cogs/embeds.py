import logging
import os
import re
from typing import Union

import discord
from discord import RawReactionActionEvent
from discord.ext import commands
from discord.ext.commands import check
from discord.utils import get

from utils.config import config
from utils.db import db
from utils.logger import logger


class Embeds(commands.Cog):
    """
    Placeholder for some extra embed logic.
    """

    @commands.command()
    @commands.has_permissions(manage_guild=True)
    async def welcome(self, ctx) -> None:
        embed = discord.Embed(
            title="Hey there!",
            description=(
                "Welcome to **/wg/ - /stpg/**, our cosy place on the discord "
                "server pile. Sit down, relax, and chat. We are friendly I "
                "promise."
            ),
            color=0xC85B5B,
        )

        if ctx.guild.icon:
            embed.set_thumbnail(url=ctx.guild.icon_url)

        embed.add_field(
            name=":green_circle: Do",
            value="Have a good time",
        )

        embed.add_field(
            name=":red_circle: Don't",
            value="Be an asshole",
        )

        embed.add_field(
            name=":goat: Goats",
            value="Are not allowed, sorry",
        )

        embed.set_footer(
            text=(
                "Do not be afraid to ask for #help, but bare in mind that we "
                "don't know everything and aren't always here."
            )
        )

        await ctx.send(embed=embed)

    @commands.command()
    @commands.has_permissions(manage_guild=True)
    async def irc(self, ctx):
        embed = discord.Embed(
            title="#wg on irc.rizon.net",
            url="https://www.rizon.net/",
            description=(
                "We have finally created an IRC " "presence for ourselves, join us!"
            ),
            color=0xC85B5B,
        )

        if ctx.guild.icon:
            embed.set_thumbnail(url=ctx.guild.icon_url)

        embed.add_field(
            name="Clients",
            value=(
                "An IRC client allows you to connect to an IRC server, there "
                "are plenty to chose from. Most people seem to be using "
                "[Hexchat](https://hexchat.github.io/), other alternatives are "
                "[irssi](https://irssi.org/) and "
                "[mibbit](https://client00.chat.mibbit.com/)."
            ),
            inline=False,
        )

        embed.add_field(
            name="Claim your nick!",
            value=(
                "Once you join Rizon and/or our channel (#wg) you will need to "
                "register your nick name, otherwise anyone can use it!"
                "The simplest way of doing this is to do "
                "`/msg NickServ REGISTER <password> <email>` "
                "If you want more information, go to "
                "[rizon's guide](https://wiki.rizon.net/index.php?title=Register_your_nickname). "
            ),
            inline=False,
        )

        embed.add_field(
            name="Hide your IP",
            value=(
                "Unless you take this step everyone in the channel will be "
                "able to see your IP address (very spooky, I know). To hide it "
                "you have to simply request a virtual host from the rizon bot "
                "by using `/msg HostServ REQUEST some.cool.fake.domain`. "
                "If you want more info, rizon has a "
                "[page for that](https://wiki.rizon.net/index.php?title=VHost)."
            ),
            inline=False,
        )

        embed.set_footer(
            text=(
                "Please try not to be an idiot. "
                "IRC channel is still moderated, "
                "our normal rules apply :)"
            )
        )

        await ctx.channel.send(embed=embed)


def setup(bot: commands.Bot):
    bot.add_cog(Embeds(bot))
