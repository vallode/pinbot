import logging
import os
import random
import re
from typing import Union

import discord
from discord import RawReactionActionEvent
from discord.ext import commands
from discord.ext.commands import check
from discord.utils import get

from utils.config import config
from utils.db import db
from utils.logger import logger


class PinBot(commands.Cog):
    """
    PinBot Cog for creating pseudo-pins in a channel.
    """

    def __init__(self, bot):
        self.bot = bot

        self.configCog = self.bot.get_cog("Config")
        self.configCog.valid_settings = [
            "pin_channel",
            "pin_emoji",
            "pin_threshold",
        ]

        self.pin_channel = config.get_or_set("pin_channel", cast="BIGINT")
        # The default pin emoji is a pushpin emoji
        self.pin_emoji = config.get_or_set("pin_emoji", default="\U0001F4CC")
        self.pin_threshold = config.get_or_set(
            "pin_threshold",
            cast="INT",
            default=2,
        )

    @commands.Cog.listener(name="on_raw_reaction_add")
    async def _on_raw_reaction_add(self, payload: RawReactionActionEvent):
        """
        When a user adds any `discord.Reaction` to a `discord.Message`

        Sends a `discord.Message` to the specified channel if it meets
        requirements.
        """
        if payload.emoji.name != self.pin_emoji:
            return

        origin_channel, message = await self._get_message_from_payload(payload)
        channel, threshold = self._get_paired_channel(origin_channel.id)

        if message.author.id == self.bot.user.id:
            return

        if self._is_pinned_message(message.id):
            return

        reaction = next(
            filter(lambda reaction: reaction.emoji == self.pin_emoji, message.reactions)
        )

        if reaction.count >= int(threshold):
            pin_embed = discord.Embed(
                timestamp=message.created_at,
                description="%s [... jump to message](%s)"
                % (message.clean_content, message.jump_url),
            )

            pin_embed.set_author(
                name="%s in #%s:" % (message.author.name, origin_channel.name),
                icon_url=message.author.avatar_url,
            )

            pin_embed.set_footer(text=self.pin_emoji)

            primary_embed = message.embeds[0] if message.embeds else None

            if message.embeds and primary_embed.type == "image":
                pin_embed.set_image(url=primary_embed.thumbnail.url)

            if message.attachments:
                pin_embed.set_image(url=message.attachments[0].url)

            pinned_message = await channel.send(embed=pin_embed)

            self._add_pinned_message(message.id, pinned_message.id)

    async def _get_message_from_payload(
        self, payload: RawReactionActionEvent
    ) -> (discord.TextChannel, discord.Message):
        """
        Get `discord.Message` from channel related to payload.
        """
        channel = self.bot.get_channel(payload.channel_id)
        message = await channel.fetch_message(payload.message_id)

        return (channel, message)

    def _get_paired_channel(self, channel_id) -> Union[None, discord.TextChannel]:
        """
        Returns a `discord.TextChannel` if a pair for the provided `channel_id`
        exists, otherwise returns `None`.
        """
        paired_channel = db.select_rows(
            (
                "SELECT paired_channel_id::bigint, threshold::int "
                "FROM paired_channel "
                "WHERE channel_id = %s::varchar"
            ),
            (channel_id,),
        )

        if not paired_channel:
            return (self.bot.get_channel(self.pin_channel), self.pin_threshold)

        if paired_channel[1]:
            return (self.bot.get_channel(paired_channel[0]), paired_channel[1])

        return (self.bot.get_channel(paired_channel[0]), self.pin_threshold)

    def _is_pinned_message(self, message_id):
        messages = db.select_rows(
            (
                "SELECT message_id "
                "FROM message "
                "WHERE message_id = %s::varchar AND pinned = true"
            ),
            (message_id,),
        )

        if messages:
            return True

        return False

    def _add_pinned_message(self, message_id, pinned_message_id):
        db.execute(
            (
                "INSERT INTO message (message_id, pinned_message_id, pinned) "
                "VALUES (%(message_id)s, %(pinned_message_id)s, true) "
                "ON CONFLICT (message_id) DO UPDATE "
                "SET pinned = true, pinned_message_id = %(pinned_message_id)s"
            ),
            {"message_id": message_id, "pinned_message_id": pinned_message_id},
        )

    @commands.command()
    async def random(self, ctx):
        """Fetches a random pinned message."""
        messages = db.select_rows(
            (
                "SELECT pinned_message_id FROM message "
                "WHERE pinned = true "
                "AND pinned_message_id IS NOT NULL "
                "ORDER BY random()"
            )
        )

        if not messages:
            return await ctx.send("Could not find any pinned messages!")

        message = int(random.choice(messages)[0])
        channel, _ = self._get_paired_channel(message)

        if not channel:
            return await ctx.send("Seems like the pin channel does not exist!")

        pinned_message = await channel.fetch_message(id=message)
        return await ctx.send(embed=pinned_message.embeds[0])

    @commands.command(name="pair")
    async def pair_channel(self, ctx, channel_id: int, threshold: int):
        db.execute(
            (
                "INSERT INTO paired_channel (channel_id, paired_channel_id, threshold) "
                "VALUES (%(channel_id)s, %(paired_channel_id)s, %(threshold)s) "
                "ON CONFLICT (channel_id) DO UPDATE "
                "SET paired_channel_id = %(paired_channel_id)s, "
                "threshold = %(threshold)s"
            ),
            {
                "channel_id": ctx.channel.id,
                "paired_channel_id": channel_id,
                "threshold": threshold,
            },
        )

        return await ctx.send("Paired channels with threshold of %s!" % threshold)


def setup(bot: commands.Bot):
    bot.add_cog(PinBot(bot))
