import discord
from discord.ext import commands

from utils.config import config
from utils.logger import logger


class Welcome(commands.Cog):
    """
    Cog to enable bot to send customised welcome messages to new users.
    """

    def __init__(self, bot):
        self.bot = bot

        self.configCog = self.bot.get_cog("Config")
        self.configCog.valid_settings = ["welcome_channel", "welcome_message"]
        self.welcome_channel = config.get_or_set("welcome_channel", cast="BIGINT")
        self.welcome_message = config.get_or_set("welcome_message")

    @commands.Cog.listener(name="on_member_join")
    async def _on_member_join(self, member: discord.Member):
        logger.info(f"User {member.name} joined")

        embed = discord.Embed(
            title=f"Welcome {member.name}",
            description=self.welcome_message,
            color=0xC85B5B,
        )

        return await self.bot.get_channel(self.welcome_channel).send(embed=embed)


def setup(bot: commands.Bot):
    bot.add_cog(Welcome(bot))
