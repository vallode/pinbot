import re

import discord
from discord.ext import commands

from utils.config import config
from utils.logger import logger


class Playing(commands.Cog):
    """
    Cog for maniplating the playing status for the bot.
    """

    def __init__(self, bot) -> None:
        self.bot = bot

        self.configCog = self.bot.get_cog("Config")
        self.configCog.valid_settings = ["playing_status"]
        self.playing_status = config.get_or_set("playing_status")

    @commands.Cog.listener(name="on_ready")
    async def _on_ready(self) -> None:
        bot_activity = discord.Activity(
            name=self.playing_status,
            type=discord.ActivityType.watching,
        )
        await self.bot.change_presence(activity=bot_activity)


def setup(bot: commands.Bot):
    bot.add_cog(Playing(bot))
