import asyncio
import re
from datetime import datetime, timedelta

import discord
from discord.ext import commands, tasks

from utils.config import config
from utils.db import db
from utils.logger import logger


def is_older_than(date: datetime, minutes: int) -> bool:
    return date <= datetime.utcnow() - timedelta(minutes=minutes)


class Topics(commands.Cog):
    """
    Cog for creating opt-in rooms designed to discuss ephemeral topics.
    i.e political discussion, current events, youtube videos...
    """

    def __init__(self, bot) -> None:
        self.bot = bot

        self.configCog = self.bot.get_cog("Config")
        self.configCog.valid_settings = [
            "topic_category",
            "topic_channel_limit",
            "join_emoji",
        ]
        self.topic_category = config.get_or_set("topic_category", "BIGINT")
        self.topic_channel_limit = config.get_or_set(
            "topic_channel_limit",
            default=2,
            cast="INT",
        )
        self.join_emoji = config.get_or_set(
            "join_emoji",
            cast="VARCHAR",
            default="\U00002795",
        )

    def cog_unload(self):
        self._delete_overdue_channels.cancel()

    @commands.Cog.listener(name="on_ready")
    async def _on_ready(self):
        self._delete_overdue_channels.start()

    @commands.Cog.listener(name="on_reaction_add")
    async def _on_reaction_add(self, reaction, user) -> None:
        """
        Adds user to topic channel role.
        """
        if not reaction.message.author.bot or user.bot:
            return

        if reaction.emoji == self.join_emoji:
            match_id = re.search(r"<#([0-9]+)>", reaction.message.embeds[0].description)
            topic_channel = self.bot.get_channel(int(match_id.group(1)))
            topic_role_id = db.select_rows(
                (
                    "SELECT role_id::bigint "
                    "FROM topic_channel "
                    "WHERE channel_id = %s::varchar"
                ),
                (topic_channel.id,),
            )
            topic_role = reaction.message.guild.get_role(topic_role_id[0])

            if topic_role in user.roles:
                return

            await user.add_roles(topic_role)
            await topic_channel.send(
                embed=discord.Embed(description=f"{user.mention} joined the channel")
            )

    @tasks.loop(minutes=10.0)
    async def _delete_overdue_channels(self) -> None:
        """
        Running every ten minutes, checks if there are channels that need to be
        deleted.

        See: https://discordpy.readthedocs.io/en/latest/ext/tasks/
        """
        logger.info("delete_overdue_channels: running...")
        topic_channels = db.select_rows(
            ("SELECT channel_id::bigint, role_id::bigint " "FROM topic_channel")
        )

        if not topic_channels:
            logger.info("delete_overdue_channels: no topic channels found.")
            return

        if not isinstance(topic_channels, list):
            topic_channels = [topic_channels]

        for channel in topic_channels:
            topic_channel = self.bot.get_channel(channel[0])

            if not topic_channel:
                db.execute(
                    (
                        "DELETE FROM topic_channel "
                        "WHERE channel_id = %(channel_id)s::varchar"
                    ),
                    {"channel_id": channel[0]},
                )
                continue

            topic_role = topic_channel.guild.get_role(channel[1])

            if topic_channel.last_message_id:
                last_time = discord.utils.snowflake_time(topic_channel.last_message_id)
            else:
                last_time = topic_channel.created_at

            if len(topic_role.members) == 0 and is_older_than(last_time, 5):
                await topic_role.delete()
                await topic_channel.delete()

                db.execute(
                    (
                        "DELETE FROM topic_channel "
                        "WHERE channel_id = %(channel_id)s::varchar"
                    ),
                    {"channel_id": channel[0]},
                )

            if is_older_than(last_time, 60):
                await topic_role.delete()
                await topic_channel.delete()

                db.execute(
                    (
                        "DELETE FROM topic_channel "
                        "WHERE channel_id = %(channel_id)s::varchar"
                    ),
                    {"channel_id": channel[0]},
                )
        logger.info("delete_overdue_channels: finished.")

    async def _create_topic_channel(
        self, guild: discord.Guild, user: discord.User, topic_name: str
    ):
        """
        Creates a channel with the requested topic name.

        """
        topic_category = self.bot.get_channel(self.topic_category)

        topic_role = await guild.create_role(
            name=topic_name,
            reason="User requested topic role.",
        )

        topic_channel = await topic_category.create_text_channel(
            topic_name[:100],
            overwrites={
                guild.default_role: discord.PermissionOverwrite(read_messages=False),
                guild.me: discord.PermissionOverwrite(read_messages=True),
                topic_role: discord.PermissionOverwrite(read_messages=True),
            },
            topic=(
                "Keep discussion to the topic at hand. Channel will be "
                "deleted after 1 hour of inactivity."
            ),
            reason="User requested topic channel.",
        )

        db.execute(
            (
                "INSERT INTO topic_channel (channel_id, role_id, created_by) "
                "VALUES (%(channel_id)s, %(role_id)s, %(created_by)s)"
            ),
            {
                "channel_id": topic_channel.id,
                "role_id": topic_role.id,
                "created_by": user.id,
            },
        )

        return topic_channel

    @commands.command()
    async def discuss(self, ctx, *, topic_name: str) -> None:
        """
        Creates a temporary channel for a `topic_name`, the channel gets
        removed if the last message is older than the one hour.

        Usage: ?discuss tom scott new video
        """
        if isinstance(ctx.message.channel, discord.DMChannel):
            return

        if not self.topic_category:
            return await ctx.send("Topic channel not set, cannot create channel!")

        user_topic_channels = db.select_rows(
            ("SELECT * " "FROM topic_channel " "WHERE created_by = %s::varchar"),
            (ctx.author.id,),
        )

        if user_topic_channels and len(user_topic_channels) >= self.topic_channel_limit:
            return await ctx.send(
                (
                    "You've created too many topic channels, "
                    "wait until one of them expires."
                )
            )

        async with ctx.channel.typing():
            topic_channel = await self._create_topic_channel(
                ctx.guild,
                ctx.author,
                topic_name,
            )

            embed = discord.Embed(
                title="Discussion channel opened",
                description=(f"Opt-in to {topic_channel.mention} by reacting below."),
                color=0xC85B5B,
            )
            embed.set_footer(text="Channel will expire after 1 hour of inactivity")
            message = await ctx.send(embed=embed)
            await message.add_reaction(self.join_emoji)

    @commands.command()
    async def leave(self, ctx) -> None:
        """
        Use in a temporary channel in order to opt-out of it.
        """
        channel_id = ctx.channel.id

        topic_role_id = db.select_rows(
            (
                "SELECT role_id::bigint "
                "FROM topic_channel "
                "WHERE channel_id = %s::varchar"
            ),
            (channel_id,),
        )

        if topic_role_id and topic_role_id[0]:
            topic_role = ctx.guild.get_role(topic_role_id[0])

            await ctx.author.remove_roles(topic_role)

    @commands.command()
    @commands.has_permissions(manage_guild=True)
    async def delete(self, ctx) -> None:
        """
        Use in a temporary channel to delete it.
        """
        topic_role_id = db.select_rows(
            (
                "SELECT role_id::bigint "
                "FROM topic_channel "
                "WHERE channel_id = %s::varchar"
            ),
            (ctx.channel.id,),
        )[0]

        topic_channel = self.bot.get_channel(ctx.channel.id)
        topic_role = topic_channel.guild.get_role(topic_role_id)

        await topic_role.delete()
        await topic_channel.delete()

        db.execute(
            ("DELETE FROM topic_channel " "WHERE channel_id = %(channel_id)s::varchar"),
            {"channel_id": ctx.channel.id},
        )


def setup(bot: commands.Bot):
    bot.add_cog(Topics(bot))
