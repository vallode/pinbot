from discord.ext import commands

from utils.config import config as configUtil
from utils.logger import logger


class Config(commands.Cog):
    """
    Cog to enable bot to send customised welcome messages to new users.
    """

    def __init__(self, bot):
        self.bot = bot

        self._valid_settings = []

    @property
    def valid_settings(self):
        return self._valid_settings

    @valid_settings.setter
    def valid_settings(self, properties: list):
        for property in properties:
            if property in self._valid_settings:
                raise ValueError(f"{property} already exists in valid settings")

            self._valid_settings.append(property)

    @commands.command()
    @commands.has_permissions(manage_guild=True)
    async def reload(self, ctx):
        """
        Reloads all extensions.
        """
        self._valid_settings.clear()
        extensions = self.bot.extensions.copy()

        for extension in extensions:
            logger.info(f"Reloading {extension}...")
            try:
                self.bot.reload_extension(extension)
            except Exception as e:
                logger.error(
                    "Failed to reload extension {}\n{}: {}".format(
                        extension,
                        type(e).__name__,
                        e,
                    )
                )
                return await ctx.send("Extension reload failed.")

        return await ctx.send("All extensions reloaded.")

    @commands.group()
    @commands.has_permissions(manage_guild=True)
    async def config(self, ctx):
        """
        Handles the server or channel permission configuration for the bot.
        """
        if ctx.invoked_subcommand is None:
            await ctx.send_help("config")

    # TODO: Validation. Validation. Validation.
    @config.command()
    async def set(self, ctx, setting_name: str, setting_value: str):
        if setting_name not in self.valid_settings:
            return await ctx.send("This setting is not currently supported!")

        configUtil.set(setting_name, setting_value)

        return await ctx.send(f"Set `{setting_name}` to `{setting_value}`")

    @config.command()
    async def get(self, ctx, setting_name: str):
        if setting_name not in self.valid_settings:
            return await ctx.send("Setting name not recognised.")

        return await ctx.send(configUtil.get(setting_name))


def setup(bot: commands.Bot):
    bot.add_cog(Config(bot))
