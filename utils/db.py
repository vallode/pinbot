import os

import psycopg2
from psycopg2 import sql

from utils.logger import logger


class Database:
    """
    PostgreSQL database interface
    """

    def __init__(self):
        self.database_url = os.getenv("DATABASE_URL")
        self.conn = None

        if not self.database_url:
            logger.critical("Database has no DATABASE_URL set.")
        else:
            self._create_tables()

    def connect(self):
        if self.conn == None:
            try:
                self.conn = psycopg2.connect(self.database_url, sslmode="require")
            except psycopg2.DatabaseError as error:
                logger.error(error)
            finally:
                logger.info("Database connection established")

    def _create_tables(self):
        tables = [
            """
            CREATE TABLE IF NOT EXISTS config (
                setting_name VARCHAR(100) PRIMARY KEY NOT NULL,
                setting_value VARCHAR(1000)
            )
            """,
            """
            CREATE TABLE IF NOT EXISTS message (
              message_id VARCHAR(1000) PRIMARY KEY NOT NULL,
              pinned_message_id VARCHAR(1000),
              pinned BOOLEAN DEFAULT false
            )
            """,
            """
            CREATE TABLE IF NOT EXISTS paired_channel (
              channel_id VARCHAR(1000) PRIMARY KEY NOT NULL,
              paired_channel_id VARCHAR(1000) NOT NULL,
              threshold INT
            )
            """,
            """
            CREATE TABLE IF NOT EXISTS topic_channel (
                channel_id VARCHAR(1000) PRIMARY KEY NOT NULL,
                role_id VARCHAR(1000) NOT NULL,
                timeout INT DEFAULT 60,
                created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                created_by VARCHAR(1000)
            )
            """,
            """
            CREATE TABLE IF NOT EXISTS discord_user (
                user_id VARCHAR(1000) PRIMARY KEY NOT NULL,
                name VARCHAR(32),
                based BOOLEAN DEFAULT false
            )
            """,
        ]

        self.connect()

        with self.conn.cursor() as cur:
            for table in tables:
                cur.execute(table)

            cur.close()

        self.conn.commit()

    def select_rows(self, query, args=None):
        """Run a SQL query to select rows from table."""
        self.connect()

        with self.conn.cursor() as cur:
            cur.execute(query, args)

            if cur.rowcount > 1:
                records = [row for row in cur.fetchall()]
            else:
                records = cur.fetchone()

            cur.close()

        return records

    def execute(self, query, args):
        self.connect()

        with self.conn.cursor() as cur:
            cur.execute(query, args)
            cur.close()

        self.conn.commit()


db = Database()
