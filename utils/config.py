import os

import psycopg2
from psycopg2 import sql

from utils.db import db
from utils.logger import logger


class Config:
    def get(self, setting_name, cast="VARCHAR"):
        config_value = db.select_rows(
            sql.SQL(
                "SELECT setting_value::{} FROM config WHERE setting_name = %s"
            ).format(sql.SQL(cast)),
            (setting_name,),
        )

        return config_value[0] if config_value else None

    def get_or_set(self, setting_name, cast="VARCHAR", default=None):
        """
        Attempts to fetch the setting value from the database, if it exists.
        If the setting name is not set, it sets it to a default value.
        """
        config_value = self.get(setting_name, cast=cast)

        if not config_value:
            self.set(setting_name, default)

        return self.get(setting_name, cast=cast)

    def set(self, setting_name, setting_value):
        db.execute(
            (
                "INSERT INTO config (setting_name, setting_value) "
                "VALUES (%(name)s, %(value)s) "
                "ON CONFLICT (setting_name) DO UPDATE "
                "SET setting_value = %(value)s"
            ),
            {
                "name": setting_name,
                "value": setting_value,
            },
        )


config = Config()
