import logging
import os
from logging.config import dictConfig

logging_config = dict(
    version=1,
    formatters={"f": {"format": "%(asctime)s:%(levelname)s - %(message)s"}},
    handlers={
        "h": {
            "class": "logging.StreamHandler",
            "formatter": "f",
            "level": logging.DEBUG,
        }
    },
    root={
        "handlers": ["h"],
        "level": logging.DEBUG if os.getenv("DEBUG") else logging.INFO,
    },
)

dictConfig(logging_config)
logger = logging.getLogger()
