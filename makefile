run:
	python3 main.py

c: check
check:
	black --check .
	isort --check .

f: format
format:
	black .
	isort .
